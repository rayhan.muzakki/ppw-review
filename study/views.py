from django.shortcuts import render
from .forms import *
from django.http import HttpResponseRedirect

# Create your views here.

def index(request):

    names = Information.objects.all()
    Informationform= Information_form()
    if(request.method=='POST'):
        Informationform= Information_form(request.POST)
        if(Informationform.is_valid()):
            Informationform.save()
            return HttpResponseRedirect('/')
        else:
            Informationform= Information_form()
    return render(request,'index.html', {'form': Informationform, 'names': names})