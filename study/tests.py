from django.test import Client, TestCase
from django.urls import resolve
from .views import index
from .models import Information
from .forms import Information_form

# Create your tests here.
class trialTest(TestCase):

    def test_url_exist(self):
        response= Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_template(self):
        response= Client().get('/')
        self.assertTemplateUsed(response,'index.html')

    def test_function(self):
        found= resolve('/')
        self.assertEqual(found.func.__name__,index.__name__)     

    def test_create_new(self):
        new= Information.objects.create(name='haew')
        self.assertTrue(isinstance(new,Information))
        self.assertTrue(new.__str__(),new.name)
        count_info= Information.objects.all().count()
        self.assertEqual(count_info,1) 

    def test_invalid_post(self):
        Client().post('/',{'name':''})
        response = Information.objects.all().count()
        self.assertEqual(response, 0)

    def test_form_new_name(self):
        Client().post('/', {'name':'help'})
        response = Information.objects.all().count()
        self.assertEqual(response, 1)
    
    